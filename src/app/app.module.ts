import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { Alunos } from '../pages/alunos/alunos';
import { Previsao } from '../pages/previsao/previsao';
import { Financeiro } from '../pages/financeiro/financeiro';
import { Filtrar } from '../pages/filtrar/filtrar';
import { Login } from '../pages/login/login';
import { Detalhes_saldo } from '../pages/detalhes_saldo/detalhes_saldo';
import { Detalhes_presencas } from '../pages/detalhes_presencas/detalhes_presencas';
import { TabsPage } from '../pages/tabs/tabs';

@NgModule({
  declarations: [
    Login,
    MyApp,
    Alunos,
    Previsao,
    Filtrar,
    Financeiro,
    Detalhes_saldo,
    Detalhes_presencas,
    TabsPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    Login,
    MyApp,
    Alunos,
    Previsao,
    Filtrar,
    Financeiro,
    Detalhes_saldo,
    Detalhes_presencas,
    TabsPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}]
})
export class AppModule {}
