import { Component } from '@angular/core';

import { NavController, LoadingController, App } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import { Login } from '../login/login';
import { Detalhes_presencas } from '../detalhes_presencas/detalhes_presencas';

@Component({
  selector: 'page-alunos',
  templateUrl: 'alunos.html'
})
export class Alunos {

  loader:any;
  dados: any = {total_alunos_dia: "-", total_alunos_ativos: "-", total_alunos_inativos: "-"};

  logout(){
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/logout', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(err),
    );
  }

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando conteúdo..."
    });
    this.loader.present().catch(() => {});
  }

  carregou() {
    this.loader.dismiss().catch(() => {});
  }

  validar_login = function(data){
    if(data.logado == 's'){
      this.dados = data;
    } else {
      this.carregou();
      this.navCtrl.pop();
      this.app.getRootNav().setRoot(Login, {
        block: true
      });
    }
    this.carregou();
  }

  erro(err){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  presencas_dia(){
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/detalhes_presencas', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.carrega(data),
      err => this.erro(err),
    );
  }

  carrega(data){
    this.carregou();
    this.navCtrl.push(Detalhes_presencas, {data: data});
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, private app:App) {
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/alunos', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(err),
    );
  }

}
