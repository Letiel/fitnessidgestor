import { Component } from '@angular/core';

import { Financeiro } from '../financeiro/financeiro';
import { Alunos } from '../alunos/alunos';
import { Previsao } from '../previsao/previsao';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  // this tells the tabs component which Pages
  // should be each tab's root Page
  Financeiro: any = Financeiro;
  Alunos: any = Alunos;
  Previsao: any = Previsao;

  constructor() {

  }
}
