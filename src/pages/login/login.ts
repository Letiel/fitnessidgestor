import { Component } from '@angular/core';

import { NavController, LoadingController, NavParams } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import {Md5} from 'ts-md5/dist/md5';

import { Storage } from '@ionic/storage';

import { TabsPage } from '../tabs/tabs';

@Component({
    selector: 'page-login',
  templateUrl: 'login.html'
})
export class Login {
  storage: Storage = new Storage();
  usuario: any;
  senha: any;
  loader:any;
  block: any = false; //variavel para bloquear a verificação de login após fazer log-out...

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando conteúdo..."
    });
    this.loader.present().catch(() => {});
  }

  carregou() {
    this.loader.dismiss().catch(() => {});
  }


  validar_login = function(data){
    if(data.logado == 's'){
      this.storage.set('usuario', data.usuario);
      this.navCtrl.pop();
      this.navCtrl.setRoot(TabsPage, {
        data: data
      });
    } else {
      this.carregou();
      alert(data.msg);
    }
    this.carregou();
  }

  testar_logado = function(data){
    if(data.logado == 's'){
      this.carregou();
      this.navCtrl.pop();
      this.navCtrl.setRoot(TabsPage, {
        data: data
      });
    }
    this.carregou();
  }

  erro(){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  entrar = function(){
    this.carregando();
    var post = "usuario=" + this.usuario + "&senha=" + Md5.hashStr(this.senha);
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/login', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(),
    );
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, public navParams: NavParams) {
    this.block = this.navParams.get('block');
    if(!this.block){
      this.carregando();
      var post = "";
      var headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/teste_login', post, {
        headers: headers
      }).map(res => res.json())
      .subscribe(
        data => this.testar_logado(data),
        err => this.erro(),
      );
    }
  }
}
