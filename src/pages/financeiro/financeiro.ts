import { Component } from '@angular/core';

import { NavController, LoadingController, App } from 'ionic-angular';
import { Filtrar } from '../filtrar/filtrar';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import { Login } from '../login/login';
import { Detalhes_saldo } from '../detalhes_saldo/detalhes_saldo';

@Component({
  selector: 'page-financeiro',
  templateUrl: 'financeiro.html'
})
export class Financeiro {

  loader:any;
  dados: any = {saldo_financeiro_dia: "-", saldo_financeiro_mes: "-", contas_a_receber: "-", contas_a_pagar: "-"};

  logout(){
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/logout', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(err),
    );
  }

  refresh(refresh){
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/financeiro', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.doRefresh(data, refresh),
      err => this.erro_app(err, refresh),
    );
  }

  erro_app(err, refresh){
    refresh.complete();
    this.erro(err);
  }

  doRefresh(data, refresh){
    refresh.complete();
    this.validar_login(data);
  }

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando conteúdo..."
    });
    this.loader.present().catch(() => {});
  }

  carregou() {
    this.loader.dismiss().catch(() => {});
  }

  validar_login = function(data){
    if(data.logado == 's'){
      this.dados = data;
    } else {
      this.carregou();
      this.navCtrl.pop();
      this.app.getRootNav().setRoot(Login, {
        block: true
      });
    }
    this.carregou();
  }

  erro(err){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, private app:App) {
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/financeiro', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(err),
    );
  }

  filtrar(){
    this.navCtrl.push(Filtrar);
  }

  saldo_dia(){
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/detalhes_saldo_dia', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.carrega_saldo(data),
      err => this.erro(err),
    );
  }

  saldo_mes(){
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/detalhes_saldo_mes', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.carrega_saldo(data),
      err => this.erro(err),
    );
  }

  carrega_saldo(data){
    this.carregou();
    this.navCtrl.push(Detalhes_saldo, {data: data});
  }

}
