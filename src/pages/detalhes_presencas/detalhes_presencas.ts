import { Component } from '@angular/core';

import { NavController, LoadingController, App, NavParams } from 'ionic-angular';

import {Http, Headers} from '@angular/http';

import 'rxjs/add/operator/map';

import { Login } from '../login/login';

@Component({
  selector: 'page-detalhes_presencas',
  templateUrl: 'detalhes_presencas.html'
})
export class Detalhes_presencas {

  loader:any;
  data_inicial: any;
  data_final: any;
  data : any;

  logout(){
    this.carregando();
    var post = "";
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/logout', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(err),
    );
  }

  erro(err){
    this.loader.dismiss();
    alert("Verifique sua conexão com a internet.");
  }

  carregando() {
    this.loader = this.loadingCtrl.create({
      content: "Carregando conteúdo..."
    });
    this.loader.present().catch(() => {});
  }

  carregou() {
    this.loader.dismiss().catch(() => {});
  }

  validar_login = function(data){
    if(data.logado == 's'){
      this.dados = data;
    } else {
      this.carregou();
      this.navCtrl.pop();
      this.app.getRootNav().setRoot(Login, {
        block: true
      });
    }
    this.carregou();
  }

  filtrar(){
    this.carregando();
    var post = "data_inicial="+this.data_inicial+"&data_final="+this.data_final;
    var headers = new Headers();
    headers.append('Content-Type', 'application/x-www-form-urlencoded');
    this.http.post('http://www.acessonaweb.com.br/Api_gerencia_fitness/filtrar', post, {
      headers: headers
    }).map(res => res.json())
    .subscribe(
      data => this.validar_login(data),
      err => this.erro(err),
    );
  }

  constructor(public navCtrl: NavController, public http: Http, public loadingCtrl: LoadingController, private app:App, private navParams: NavParams) {
    this.data = navParams.get('data');
    console.log(this.data);
  }

}
