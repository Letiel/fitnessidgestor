/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.ionicframework.fitnessidgestor772996;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.ionicframework.fitnessidgestor772996";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 507;
  public static final String VERSION_NAME = "0.5.7";
}
